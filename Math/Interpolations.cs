﻿/*----------------------------------------------------------------------------------
 *
 * Author: Andrew Milsom, @AJMilsom, Toggle Play Ltd
 * Email: andrew@toggleplay.com
 * Summery:
 * 
//--------------------------------------------------------------------------------*/

using UVector2 = UnityEngine.Vector2;
using UVector3 = UnityEngine.Vector3;
using UVector4 = UnityEngine.Vector4;
using UColor = UnityEngine.Color;
using Math = UnityEngine.Mathf;

namespace TogglePlay
{

	public enum InterpolationMode
	{
		Lerp = 0,
		SmoothStep,
		Berp,
		Hermite,
		Sinerp,
		Coserp,
		Clerp
	}

	public static class Interpolation
	{
		public static UVector2 Vector2( UVector2 start, UVector2 end, float t, InterpolationMode method )
		{
			return new UVector2( Float( start.x, end.x, t, method ),
			                                 Float( start.y, end.y, t, method ) );
		}

		public static UVector3 Vector3( UVector3 start, UVector3 end, float t, InterpolationMode method )
		{
			return new UVector3( Float( start.x, end.x, t, method ),
			                    Float( start.y, end.y, t, method ),
			                    Float( start.z, end.z, t, method ) );
		}

		public static UVector4 Vector4( UVector4 start, UVector4 end, float t, InterpolationMode method )
		{
			return new UVector4( Float( start.x, end.x, t, method ),
			                    Float( start.y, end.y, t, method ),
			                    Float( start.z, end.z, t, method ),
			                    Float( start.w, end.w, t, method ) );
		}

		// Treat as Vector3, some methods will not work so well with colors
		public static UColor Color( UColor start, UColor end, bool interpolateAlpha, float t, InterpolationMode method )
		{
			UColor retValue = UColor.white;
			retValue.r = Float( start.r, end.r, t, method );
			retValue.g = Float( start.g, end.g, t, method );
			retValue.b = Float( start.b, end.b, t, method );
			if( interpolateAlpha )
				retValue.a = Float( start.a, end.a, t, method );
			else
				retValue.a = start.a;
			return retValue;
		}

	    public static float Float( float start, float end, float t, InterpolationMode method )
	    {
	        float retValue = start;
	        switch(method)
	        {
	            case InterpolationMode.Hermite: retValue = Hermite(start, end, t); break;
	            case InterpolationMode.Sinerp:
	                retValue = Sinerp(start, end, t);
	                break;
	            case InterpolationMode.Coserp:
	                retValue = Coserp(start, end, t);
	                break;
	            case InterpolationMode.Berp:
	                retValue = Berp(start, end, t);
	                break;
	            case InterpolationMode.Lerp:
	                retValue = Math.Lerp(start, end, t);
					break;
				case InterpolationMode.SmoothStep:
					retValue = Math.SmoothStep(start, end, t);
	                break;
				case InterpolationMode.Clerp:
					retValue = Clerp(start, end, t);
					break;
	        }

	        return retValue;
	    }

	    public static float Hermite(float start, float end, float t)
	    {
			return Math.Lerp(start, end, t * t * (3.0f - 2.0f * t));
	    }

	    public static float Sinerp(float start, float end, float t)
	    {
			return Math.Lerp(start, end, Math.Sin(t * Math.PI * 0.5f));
	    }

	    public static float Coserp(float start, float end, float t)
	    {
			return Math.Lerp(start, end, 1.0f - Math.Cos(t * Math.PI * 0.5f));
	    }

	    public static float Berp(float start, float end, float t)
	    {
	        t = Math.Clamp01(t);
	        t = (Math.Sin(t * Math.PI * (0.2f + 2.5f * t * t * t)) * Math.Pow(1f - t, 2.2f) + t) * (1f + (1.2f * (1f - t)));
	        return start + (end - start) * t;
	    }
		
		const float CLERPMIN = 0;
		const float CLERPMAX = 360f;
		public static float Clerp(float start , float end, float t)
		{
			float half = Math.Abs((CLERPMAX - CLERPMIN)/2.0f);
			
			if((end - start) < -half)
			{
				float diff = ((CLERPMAX - start)+end)*t;
				return start+diff;
			}
			else if((end - start) > half)
			{
				float diff = -((CLERPMAX - end)+start)*t;
				return start+diff;
			}
			else
				return start+(end-start)*t;
		}

		public static float Clerp(float start , float end, float t, float circularMin, float circularMax )
		{
			float half = Math.Abs((circularMax - circularMin)/2.0f);
			
			if((end - start) < -half)
			{
				float diff = ((circularMax - start)+end)*t;
				return start+diff;
			}
			else if((end - start) > half)
			{
				float diff = -((circularMax - end)+start)*t;
				return start+diff;
			}
			else
				return start+(end-start)*t;
		}
	}

}

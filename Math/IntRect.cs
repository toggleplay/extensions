/*----------------------------------------------------------------------------------
 *
 * Author: Andrew Milsom, @AJMilsom, Toggle Play Ltd
 * Email: andrew@toggleplay.com
 * Summery:
 * 
//--------------------------------------------------------------------------------*/
using UnityEngine;
using System.Collections;

/// <summary>
/// Int rect. Duplicate of UnityEngine.Rect but using int
/// </summary>
public struct IntRect
{
	private int m_XMin;
	private int m_YMin;
	private int m_Width;
	private int m_Height;
	public int x
	{
		get
		{
			return this.m_XMin;
		}
		set
		{
			this.m_XMin = value;
		}
	}

	public int y
	{
		get
		{
			return this.m_YMin;
		}
		set
		{
			this.m_YMin = value;
		}
	}
	public Vector2 center
	{
		get
		{
			return new Vector2 ((float)(this.x + this.m_Width) / 2f, (float)(this.y + this.m_Height) / 2f);
		}
		set
		{
			this.m_XMin = (int)(value.x - (float)this.m_Width / 2f);
			this.m_YMin = (int)(value.y - (float)this.m_Height / 2f);
		}
	}
	public int width
	{
		get
		{
			return this.m_Width;
		}
		set
		{
			this.m_Width = value;
		}
	}
	public int height
	{
		get
		{
			return this.m_Height;
		}
		set
		{
			this.m_Height = value;
		}
	}
	[System.Obsolete("use xMin")]
	public int left
	{
		get
		{
			return this.m_XMin;
		}
	}
	[System.Obsolete ("use xMax")]
	public float right
	{
		get
		{
			return this.m_XMin + this.m_Width;
		}
	}
	[System.Obsolete ("use yMin")]
	public int top
	{
		get
		{
			return this.m_YMin;
		}
	}
	[System.Obsolete ("use yMax")]
	public int bottom
	{
		get
		{
			return this.m_YMin + this.m_Height;
		}
	}
	public int xMin
	{
		get
		{
			return this.m_XMin;
		}
		set
		{
			int xMax = this.xMax;
			this.m_XMin = value;
			this.m_Width = xMax - this.m_XMin;
		}
	}
	public int yMin
	{
		get
		{
			return this.m_YMin;
		}
		set
		{
			int yMax = this.yMax;
			this.m_YMin = value;
			this.m_Height = yMax - this.m_YMin;
		}
	}
	public int xMax
	{
		get
		{
			return this.m_Width + this.m_XMin;
		}
		set
		{
			this.m_Width = value - this.m_XMin;
		}
	}
	public int yMax
	{
		get
		{
			return this.m_Height + this.m_YMin;
		}
		set
		{
			this.m_Height = value - this.m_YMin;
		}
	}
	public IntRect (int left, int top, int width, int height)
	{
		this.m_XMin = left;
		this.m_YMin = top;
		this.m_Width = width;
		this.m_Height = height;
	}
	public IntRect (IntRect source)
	{
		this.m_XMin = source.m_XMin;
		this.m_YMin = source.m_YMin;
		this.m_Width = source.m_Width;
		this.m_Height = source.m_Height;
	}
	public static IntRect MinMaxRect (int left, int top, int right, int bottom)
	{
		return new IntRect (left, top, right - left, bottom - top);
	}
	public void Set (int left, int top, int width, int height)
	{
		this.m_XMin = left;
		this.m_YMin = top;
		this.m_Width = width;
		this.m_Height = height;
	}
	public override string ToString ()
	{
		return string.Format ("(left:{0:F2}, top:{1:F2}, width:{2:F2}, height:{3:F2})", new object[]
		{
			this.x,
			this.y,
			this.width,
			this.height
		});
	}
	public string ToString (string format)
	{
		return string.Format ("(left:{0}, top:{1}, width:{2}, height:{3})", new object[]
		{
			this.x.ToString (format),
			this.y.ToString (format),
			this.width.ToString (format),
			this.height.ToString (format)
		});
	}
	public bool Contains (Vector2 point)
	{
		return point.x >= this.xMin && point.x < this.xMax && point.y >= this.yMin && point.y < this.yMax;
	}
	public bool Contains(int x, int y)
	{
		return x >= this.xMin && x < this.xMax && y >= this.yMin && y < this.yMax;
	}

	public bool Contains (Vector3 point)
	{
		return point.x >= this.xMin && point.x < this.xMax && point.y >= this.yMin && point.y < this.yMax;
	}
	
	public bool Contacts( IntRect other )
	{
		if( other.xMax <= xMin ||
			other.xMin >= xMax ||
			other.yMax <= yMin ||
			other.yMin >= yMax )
			return false;
		return true;
	}
	
	public override int GetHashCode ()
	{
		return this.x.GetHashCode () ^ this.width.GetHashCode () << 2 ^ this.y.GetHashCode () >> 2 ^ this.height.GetHashCode () >> 1;
	}

	public override bool Equals (object other)
	{
		if (!(other is IntRect))
		{
			return false;
		}
		IntRect rect = (IntRect)other;
		return this.x.Equals (rect.x) && this.y.Equals (rect.y) && this.width.Equals (rect.width) && this.height.Equals (rect.height);
	}

	public bool CanFit( IntRect other )
	{
		if( Contains(other.xMin, other.yMin) && Contains(other.xMax, other.yMax) )
			return true;
		return false;
	}
	
	public bool CanFit( int fitwidth, int fitheight )
	{
		if( width >= fitwidth && height >= fitheight )
			return true;
		return false;
	}
		
	public static bool operator != (IntRect lhs, IntRect rhs)
	{
		return lhs.x != rhs.x || lhs.y != rhs.y || lhs.width != rhs.width || lhs.height != rhs.height;
	}

	public static bool operator == (IntRect lhs, IntRect rhs)
	{
		return lhs.x == rhs.x && lhs.y == rhs.y && lhs.width == rhs.width && lhs.height == rhs.height;
	}
}

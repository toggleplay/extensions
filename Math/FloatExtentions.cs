/*----------------------------------------------------------------------------------
 *
 * Author: Andrew Milsom, @AJMilsom, Toggle Play Ltd
 * Email: andrew@toggleplay.com
 * Summery:
 * 
//--------------------------------------------------------------------------------*/

using UnityEngine;

public static class FloatExtentions
{
	public static bool IsWithinRange( this float val, float min, float max )
    {
        return (val >= min) && (val <= max);
    }

	public static bool IsWithinRangeOf( this float val, float about, float range )
	{
		return Mathf.Abs(val - about) < range;
	}

	public static float DeltaTo( this float val, float other )
	{
		return Mathf.Abs( val - other );
	}
	
	public static float GetClampedEuler( this float a, float min, float max )
	{
		if( min > max )
		{
			Debug.LogWarning("clamping angle where min is greater than max: returning min");
			return min;
		}
		
		a = GetWrap180(a);
		
		if( a < min )
		{
			return min;
		}
		else if( a > max )
		{
			return max;
		}
		
		return a;
	}
	
	public static float GetWrap180( this float a )
	{
		if( a < -180 || a > 360 )
			a = GetWrap360(a);

		if( a >-180 && a < 180 )
			return a;

		a -= 360;
		return a;
	}
	
	public static float GetWrap360( this float a )
	{
		if( a > 360 || a < -360 )
			a = a % 360f;

		if( a < 0 )
			a = a+360f;

		return a;
	}
}

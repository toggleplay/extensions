/*----------------------------------------------------------------------------------
 *
 * Author: Andrew Milsom, @AJMilsom, Toggle Play Ltd
 * Email: andrew@toggleplay.com
 * Summery:
 * 
//--------------------------------------------------------------------------------*/

using UnityEngine;

public static class Vector2Extentions
{
	public static bool Approximately( this Vector2 v1, Vector2 v2 )
	{
		if( Mathf.Approximately(v1.x,v2.x) &&
		    Mathf.Approximately(v1.y,v2.y) )
			return true;
		
		return false;
	}
	public static bool Approximately( this Vector2 v1, float _x, float _y )
	{
		if( Mathf.Approximately(v1.x,_x) &&
		    Mathf.Approximately(v1.y,_y) )
			return true;
		
		return false;
	}
	
	public static string ToStringFull( this Vector2 v1 )
	{
		return "(x:" + v1.x + ", y:" + v1.y + ")";
	}
	
	public static bool IsWithinSquare( this Vector2 v1, Vector2 center, Vector2 size )
	{
		Vector2 min = new Vector2(center.x - size.x, center.y - size.y );
		Vector2 max = new Vector2(center.x + size.x, center.y + size.y );
		if( v1.x.IsWithinRange(min.x, max.x) && v1.y.IsWithinRange(min.y, max.y) )
			return true;
		return false;
	}

	
	public static Vector2 UnitMultiply(this Vector2 myVector, Vector2 other )
	{
		myVector.x *= other.x;
		myVector.y *= other.y;
		
		return myVector;
	}

	public static Vector2 UnitDivide(this Vector2 myVector, Vector2 other )
	{
		myVector.x /= other.x;
		myVector.y /= other.y;
		
		return myVector;
	}

	public static Vector2 UnitAdd(this Vector2 myVector, Vector2 other )
	{
		myVector.x += other.x;
		myVector.y += other.y;
		
		return myVector;
	}
}

/*----------------------------------------------------------------------------------
 *
 * Author: Andrew Milsom, @AJMilsom, Toggle Play Ltd
 * Email: andrew@toggleplay.com
 * Summery:
 * 
//--------------------------------------------------------------------------------*/

using UnityEngine;

public static class Vector3Extentions
{
	public static bool Approximately( this Vector3 v1, Vector3 v2 )
	{
		if( Mathf.Approximately(v1.x,v2.x) &&
		    Mathf.Approximately(v1.y,v2.y) &&
		    Mathf.Approximately(v1.z,v2.z) )
			return true;
		
		return false;
	}
	public static bool Approximately( this Vector3 v1, float _x, float _y, float _z )
	{
		if( Mathf.Approximately(v1.x,_x) &&
		    Mathf.Approximately(v1.y,_y) &&
		    Mathf.Approximately(v1.z,_z) )
			return true;
		
		return false;
	}
	
	public static string ToStringFull( this Vector3 v1 )
	{
		return "(x:" + v1.x + ", y:" + v1.y + ", z:" + v1.z + ")";
	}
	
	public static bool IsWithinCube( this Vector3 v1, Vector3 center, Vector3 size )
	{
		Vector3 min = new Vector3(center.x - size.x, center.y - size.y, center.z - size.z );
		Vector3 max = new Vector3(center.x + size.x, center.y + size.y, center.z + size.z );

		if( v1.x.IsWithinRange(min.x, max.x) && v1.y.IsWithinRange(min.y, max.y) && v1.z.IsWithinRange(min.z, max.z) )
			return true;

		return false;
	}

	public static bool IsWithinRangeOf( this Vector3 v1, Vector3 about, float range)
	{
		return (v1 - about).sqrMagnitude < range*range;
	}
	
	
	public static float AngleTo( this Vector3 fromVector, Vector3 toVector, Vector3 up)
	{
		float angle = Vector3.Angle(fromVector, toVector);
		
		Vector3 crossDir = Vector3.Cross(fromVector, toVector);
		float dir = Vector3.Dot(crossDir, up);
		
		if (dir > 0f)
			return angle;
		else if (dir < 0f)
			return -angle;
		else
			return 0f;
	}
	
	public static Vector3 Rotate( this Vector3 myVector, Vector3 euler )
	{
		Matrix4x4 m = Matrix4x4.TRS(Vector3.zero, Quaternion.Euler(euler.x,euler.y,euler.z), Vector3.one);
		Vector4 v4 = new Vector4(myVector.x, myVector.y, myVector.z, 1);
		v4 = m*v4;
		myVector.x = v4.x;
		myVector.y = v4.y;
		myVector.z = v4.z;

		return myVector;
	}
	
	public static Vector3 UnitMultiply( this Vector3 myVector, Vector3 other )
	{
		myVector.x *= other.x;
		myVector.y *= other.y;
		myVector.z *= other.z;
		
		return myVector;
	}
	public static Vector3 UnitDivide( this Vector3 myVector, Vector3 other )
	{
		myVector.x /= other.x;
		myVector.y /= other.y;
		myVector.z /= other.z;
		
		return myVector;
	}
	public static Vector3 UnitAdd( this Vector3 myVector, float toAdd )
	{
		myVector.x += toAdd;
		myVector.y += toAdd;
		myVector.z += toAdd;
		
		return myVector;
	}
}

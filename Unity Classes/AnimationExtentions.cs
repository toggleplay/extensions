/*----------------------------------------------------------------------------------
 *
 * Author: Andrew Milsom, @AJMilsom, Toggle Play Ltd
 * Email: andrew@toggleplay.com
 * Summery:
 * 
//--------------------------------------------------------------------------------*/

using UnityEngine;

public static class AnimationExtentions
{
	public static string GetMostWeightedAnimation( this Animation me )
	{
		float mostWeighted = -1f;
		string playing = string.Empty;
		
		foreach( AnimationState s in me )
		{
		    if( s.enabled == true && s.weight > mostWeighted )
			{
		        playing = s.name;
		        mostWeighted = s.weight;
		    }
		}
		
		return playing;
	}

}
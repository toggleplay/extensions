/*----------------------------------------------------------------------------------
 *
 * Author: Andrew Milsom, @AJMilsom, Toggle Play Ltd
 * Email: andrew@toggleplay.com
 * Summery:
 * 
//--------------------------------------------------------------------------------*/

using UnityEngine;

public static class ColorExtentions
{

	public static Color TakeRGBFrom( this Color col, Color other )
	{
		col.r = other.r;
		col.g = other.g;
		col.b = other.b;
		return col;
	}

	public static Color TakeAlphaFrom( this Color col, Color other )
	{
		col.a = other.a;
		return col;
	}
	
	public static Color AsGrayscale( this Color col )
	{
		float g = 0.2989f*col.r + 0.587f*col.g + 0.114f*col.b;
		return new Color(g,g,g, col.a);
	}

}
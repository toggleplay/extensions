/*----------------------------------------------------------------------------------
 *
 * Author: Andrew Milsom, @AJMilsom, Toggle Play Ltd
 * Email: andrew@toggleplay.com
 * Summery:
 * 
//--------------------------------------------------------------------------------*/

using UnityEngine;

public static class TransformExtentions
{
	public static void SetPositionX( this Transform v1, float x )
	{
		Vector3 p = v1.position;
		p.x = x;
		v1.position = p;
	}
	public static void SetPositionY( this Transform v1, float y )
	{
		Vector3 p = v1.position;
		p.y = y;
		v1.position = p;
	}
	public static void SetPositionZ( this Transform v1, float z )
	{
		Vector3 p = v1.position;
		p.z = z;
		v1.position = p;
	}
	
	public static void SetLocalPosX( this Transform v1, float x )
	{
		Vector3 p = v1.localPosition;
		p.x = x;
		v1.localPosition = p;
	}
	public static void SetLocalPosY( this Transform v1, float y )
	{
		Vector3 p = v1.localPosition;
		p.y = y;
		v1.localPosition = p;
	}
	public static void SetLocalPosZ( this Transform v1, float z )
	{
		Vector3 p = v1.localPosition;
		p.z = z;
		v1.localPosition = p;
	}
	
	public static void SetScaleX( this Transform v1, float x )
	{
		Vector3 p = v1.localScale;
		p.x = x;
		v1.localScale = p;
	}
	public static void SetScaleY( this Transform v1, float y )
	{
		Vector3 p = v1.localScale;
		p.y = y;
		v1.localScale = p;
	}
	public static void SetScaleZ( this Transform v1, float z )
	{
		Vector3 p = v1.localScale;
		p.z = z;
		v1.localScale = p;
	}
	
	public static Vector3 GetRotationEulerClamped180( this Transform t )
	{
		Vector3 rot = t.rotation.eulerAngles;
		rot.x = rot.x.GetWrap180();
		rot.y = rot.y.GetWrap180();
		rot.z = rot.z.GetWrap180();
		return rot;
	}
	public static Vector3 GetLocalRotationEulerClamped180( this Transform t )
	{
		Vector3 rot = t.localRotation.eulerAngles;
		rot.x = rot.x.GetWrap180();
		rot.y = rot.y.GetWrap180();
		rot.z = rot.z.GetWrap180();
		return rot;
	}
}
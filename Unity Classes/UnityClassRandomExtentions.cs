﻿/*----------------------------------------------------------------------------------
 *
 * Author: Andrew Milsom, @AJMilsom, Toggle Play Ltd
 * Email: andrew@toggleplay.com
 * Summery:
 * 
//--------------------------------------------------------------------------------*/

using UnityEngine;
using System.Collections;

using URandom = UnityEngine.Random;

public static class UnityClassRandomExtentions
{
	public static Vector3 RandomWorldPointInside( this BoxCollider me )
	{
		Vector3 offset = me.center;
		Vector3 r = me.size; // TODO needs testing to see if needs halving
		
		offset += new Vector3( Random.Range(-r.x,r.x), Random.Range(-r.y,r.y), Random.Range(-r.z,r.z) );
		
		Matrix4x4 m = me.transform.localToWorldMatrix;
		Vector4 v4 = new Vector4(offset.x, offset.y, offset.z, 1);
		v4 = m*v4;
		
		return new Vector3(v4.x,v4.y,v4.z);
	}

	public static Vector3 RandomWorldPointInside( this SphereCollider me )
	{
		Vector3 offset = me.center;
		float r = Random.Range(0f, me.radius);
		
		Vector3 d = new Vector3( Random.Range(-1f,1f), Random.Range(-1f,1f), Random.Range(-1f,1f) );
		d = d.normalized*r;
		
		offset += d;
		Matrix4x4 m = me.transform.localToWorldMatrix;
		Vector4 v4 = new Vector4(offset.x, offset.y, offset.z, 1);
		v4 = m*v4;
		
		return new Vector3(v4.x,v4.y,v4.z);
	}

	//----------------------------------------------------------------------------------
	// Random Vector2 functions
	//----------------------------------------------------------------------------------

	/// <summary>
	/// Sets the Vector2 to random values between 0 and 1.
	/// </summary>
	/// <returns>self.</returns>
	public static Vector2 SetToRandom( this Vector2 v )
	{
		v = new Vector2( URandom.value, URandom.value );
		return v;
	}

	/// <summary>
	/// Sets the Vector2 to random values within from and to.
	/// </summary>
	/// <returns>self.</returns>
	/// <param name="fromVector">From vector valies.</param>
	/// <param name="toVector">To vector values.</param>
	public static Vector2 SetToRandom( this Vector2 v, Vector2 fromVector, Vector2 toVector )
	{
		v = new Vector2( URandom.Range(fromVector.x, toVector.x),
		                URandom.Range(fromVector.y, toVector.y) );
		return v;
	}

	/// <summary>
	/// Gets a random values between 0 and 1.
	/// </summary>
	/// <returns>The random Vector2.</returns>
	public static Vector2 RandomVector2( )
	{
		return new Vector2( URandom.value, URandom.value );
	}

	/// <summary>
	/// Gets a random Vector2 with values within from and to.
	/// </summary>
	/// <returns>The random Vector2.</returns>
	/// <param name="fromVector">From vector values.</param>
	/// <param name="toVector">To vector values.</param>
	public static Vector2 RandomVector2( Vector2 fromVector, Vector2 toVector )
	{
		return new Vector2( URandom.Range(fromVector.x, toVector.x),
		                   URandom.Range(fromVector.y, toVector.y) );
	}

	//----------------------------------------------------------------------------------
	// Random Vector3 functions
	//----------------------------------------------------------------------------------

	/// <summary>
	/// Sets the Vector3 to random values between 0 and 1.
	/// </summary>
	/// <returns>self.</returns>
	public static Vector3 SetToRandom( this Vector3 v )
	{
		v = new Vector3( URandom.value, URandom.value, URandom.value );
		return v;
	}

	/// <summary>
	/// Sets the Vector3 to random values within from and to.
	/// </summary>
	/// <returns>self.</returns>
	/// <param name="fromVector">From vector values.</param>
	/// <param name="toVector">To vector values.</param>
	public static Vector3 SetToRandom( this Vector3 v, Vector3 fromVector, Vector3 toVector )
	{
		v = new Vector3( URandom.Range(fromVector.x, toVector.x),
		                   URandom.Range(fromVector.y, toVector.y),
		                   URandom.Range(fromVector.z, toVector.z) );
		return v;
	}

	/// <summary>
	/// Gets a random vector3 with random values within 0 and 1.
	/// </summary>
	/// <returns>The random vector3.</returns>
	public static Vector3 RandomVector3( )
	{
		return new Vector3( URandom.value, URandom.value, URandom.value );
	}

	/// <summary>
	/// Gets a random Vector3 with values within from and to.
	/// </summary>
	/// <returns>The random Vector3.</returns>
	/// <param name="fromVector">From vector values.</param>
	/// <param name="toVector">To vector values.</param>
	public static Vector3 RandomVector3( Vector3 fromVector, Vector3 toVector )
	{
		return new Vector3( URandom.Range(fromVector.x, toVector.x), 
		                   URandom.Range(fromVector.y, toVector.y), 
		                   URandom.Range(fromVector.z, toVector.z) );
	}

	//----------------------------------------------------------------------------------
	// Random Vector4 functions
	//----------------------------------------------------------------------------------

	/// <summary>
	/// Sets the Vector4 to random values between 0 and 1 (including w)
	/// </summary>
	/// <returns>self.</returns>
	public static Vector4 SetToRandom( this Vector4 v )
	{
		v = new Vector4( URandom.value, URandom.value, URandom.value, URandom.value );
		return v;
	}

	/// <summary>
	/// Sets the Vector4 to random values within from and to.
	/// </summary>
	/// <returns>self.</returns>
	/// <param name="fromVector">From vector values.</param>
	/// <param name="toVector">To vector values.</param>
	public static Vector4 SetToRandom( this Vector4 v, Vector4 fromVector, Vector4 toVector )
	{
		v = new Vector4( URandom.Range(fromVector.x, toVector.x),
		                   URandom.Range(fromVector.y, toVector.y),
		                   URandom.Range(fromVector.z, toVector.z),
		                   URandom.Range(fromVector.w, toVector.w));
		return v;
	}

	/// <summary>
	/// Gets a random vector4 with random values within 0 and 1.
	/// </summary>
	/// <returns>The random vector4.</returns>
	public static Vector4 RandomVector4( )
	{
		return new Vector4( URandom.value, URandom.value, URandom.value, URandom.value );
	}

	/// <summary>
	/// Gets a random Vector4 with values within from and to.
	/// </summary>
	/// <returns>The random Vector4.</returns>
	/// <param name="fromVector">From vector values.</param>
	/// <param name="toVector">To vector values.</param>
	public static Vector4 RandomVector4( Vector4 fromVector, Vector4 toVector )
	{
		return new Vector4( URandom.Range(fromVector.x, toVector.x), 
		                   URandom.Range(fromVector.y, toVector.y), 
		                   URandom.Range(fromVector.z, toVector.z),
		                   URandom.Range(fromVector.w, toVector.w) );
	}

	//----------------------------------------------------------------------------------
	// Random Quaternion functions
	//----------------------------------------------------------------------------------

	/// <summary>
	/// Sets Quaternion to random values and returns it.
	/// </summary>
	/// <returns>self.</returns>
	public static Quaternion SetToRandom( this Quaternion v )
	{
		v = URandom.rotation;
		return v;
	}

	/// <summary>
	/// Sets the quaternion to random values between fromQuat and toQuat and returns it.
	/// </summary>
	/// <returns>self.</returns>
	/// <param name="fromQuat">From quaternion values.</param>
	/// <param name="toQuat">To quaternion values.</param>
	public static Quaternion SetToRandom( this Quaternion v, Quaternion fromQuat, Quaternion toQuat )
	{
		v = Quaternion.Slerp(fromQuat, toQuat, URandom.value);
		return v;
	}

	/// <summary>
	/// Returns a random quaternion.
	/// </summary>
	/// <returns>The random quaternion.</returns>
	public static Quaternion RandomQuaternion( )
	{
		return URandom.rotation;
	}

	/// <summary>
	/// Returns a random quaternion with values between fromQuat and toQuat.
	/// </summary>
	/// <returns>The random quaternion.</returns>
	/// <param name="fromQuat">From quaternion values.</param>
	/// <param name="toQuat">To quaternion values.</param>
	public static Quaternion RandomQuaternion( Quaternion fromQuat, Quaternion toQuat )
	{
		return Quaternion.Slerp(fromQuat, toQuat, URandom.value);
	}

	//----------------------------------------------------------------------------------
	// Random Colour functions
	//----------------------------------------------------------------------------------

	/// <summary>
	/// Sets the Color to random values and returns it.
	/// </summary>
	/// <returns>self.</returns>
	public static Color SetToRandom( this Color c )
	{
		c = new Color( URandom.value, URandom.value, URandom.value, URandom.value );
		return c;
	}

	/// <summary>
	/// Sets the Color to random values and returns it.
	/// </summary>
	/// <returns>self.</returns>
	/// <param name="includeAlpha">If set to <c>true</c> include alpha.</param>
	public static Color SetToRandom( this Color c, bool includeAlpha )
	{
		c = new Color( URandom.value, URandom.value, URandom.value, includeAlpha ? URandom.value : c.a );
		return c;
	}

	/// <summary>
	/// Sets the Color to random values between fromColor and toColor and returns it.
	/// </summary>
	/// <returns>self.</returns>
	/// <param name="fromColor">From color values.</param>
	/// <param name="toColor">To color values.</param>
	public static Color SetToRandom( this Color c, Color fromColor, Color toColor )
	{
		c = new Color( URandom.Range(fromColor.r, toColor.r),
		                URandom.Range(fromColor.g, toColor.g),
		                URandom.Range(fromColor.b, toColor.b),
		                URandom.Range(fromColor.a, toColor.a));
		return c;
	}

	/// <summary>
	/// Gets a random color.
	/// </summary>
	/// <returns>The random color.</returns>
	public static Color RandomColor( )
	{
		return new Color( URandom.value, URandom.value, URandom.value, URandom.value );
	}

	/// <summary>
	/// Gets a randoms color.
	/// </summary>
	/// <returns>The random color.</returns>
	/// <param name="includeAlpha">If set to <c>true</c> include alpha.</param>
	public static Color RandomColor( bool includeAlpha )
	{
		return new Color( URandom.value, URandom.value, URandom.value, includeAlpha ? URandom.value : 1 );
	}

	/// <summary>
	/// Gets a random color between the colors fromColor and toColor.
	/// </summary>
	/// <returns>The random color.</returns>
	/// <param name="fromColor">From color values.</param>
	/// <param name="toColor">To color values.</param>
	public static Color RandomColor( Color fromColor, Color toColor )
	{
		return new Color( URandom.Range(fromColor.r, toColor.r), 
		                   URandom.Range(fromColor.g, toColor.g), 
		                   URandom.Range(fromColor.b, toColor.b),
		                   URandom.Range(fromColor.a, toColor.a) );
	}
}

/*----------------------------------------------------------------------------------
 *
 * Author: Andrew Milsom, @AJMilsom, Toggle Play Ltd
 * Email: andrew@toggleplay.com
 * Summery:
 * 
//--------------------------------------------------------------------------------*/

using UnityEngine;

public static class MaterialExtentions
{
	public static void SetAlpha( this Material mat, float a )
	{
		Color c = mat.color;
		c.a = a;
		mat.color = c;
	}
	
	public static void SetRGB( this Material mat, Color fromCol )
	{
		Color c = mat.color;
		c = c.TakeRGBFrom(fromCol);
		mat.color = c;
	}
	
	public static void SetR( this Material mat, float r )
	{
		Color c = mat.color;
		c.r = r;
		mat.color = c;
	}
	public static void SetG( this Material mat, float g )
	{
		Color c = mat.color;
		c.g = g;
		mat.color = c;
	}
	public static void SetB( this Material mat, float b )
	{
		Color c = mat.color;
		c.b = b;
		mat.color = c;
	}
}
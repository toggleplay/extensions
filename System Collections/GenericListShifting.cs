﻿/*----------------------------------------------------------------------------------
 *
 * Author: Andrew Milsom, @AJMilsom, Toggle Play Ltd
 * Email: andrew@toggleplay.com
 * Summery:
 * 
//--------------------------------------------------------------------------------*/

using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public static class GenericsExtentions
{
	/// <summary>
	/// Shifts the element at index higher in the list (index+1)
	/// </summary>
	public static bool ShiftUp<T>( this List<T> list, int index )
	{
		// if its the last in the list, ignore
		if( index < 0 || list.Count-1 <= index )
			return false;

		T temp = list[index+1];
		list[index+1] = list[index];
		list[index] = temp;
		return true;
	}

	/// <summary>
	/// Shifts the element at index lower in the list (index-1)
	/// </summary>
	public static bool ShiftDown<T>( this List<T> list, int index )
	{
		// if its the last in the list, ignore
		if( index <= 0 || list.Count <= index )
			return false;
		
		T temp = list[index-1];
		list[index-1] = list[index];
		list[index] = temp;
		return true;
	}

	public static void MoveToEnd<T>( this List<T> list, int index )
	{
		if( index < 0 || list.Count-1 <= index )
			return;
		
		T temp = list[list.Count-1];
		list[list.Count-1] = list[index];
		list[index] = temp;
	}

	public static void MoveToStart<T>( this List<T> list, int index )
	{
		if( index <= 0 || list.Count <= index )
			return;
		
		T temp = list[0];
		list[0] = list[index];
		list[index] = temp;
	}
}
